 

      // 3. This function creates an <iframe> (and YouTube player)
      //    after the API code downloads.
      var player;
      
      
      function setupPlayer1(vid ) {
        player = new YT.Player('player', {
          height: '390',
          width: '640',
          videoId: 'vid',
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
        });
        
        //set loop time event only when ready
        startInterval();
        
      }
      
 
     
      function tick(){
      	ticks.push(currtime_1);
      	ticks.sort(function(a, b){return a-b});
      	updateData();
      }
      
      function updateData(){
      	infoText = ticks.join("<br />");
      	document.getElementById("data_column").innerHTML=infoText;
      	for( i=0; i<(ticks).length; i++){
			if ( currtime_1 < ticks[i] ){
			 	hiliteTick(Math.max(i-1, 0));
			 	break;
 			}
		}
      	
      }
      
      function nextPlayLoop(){
      	play_section++;
      	console.log(play_section);
      	hiliteTick(play_section);
      }
      
      function hiliteTick(n){
      	if ( n < 0 ){return;}
      	
      	var ticks_tmp = ticks.slice(0);
      	tmp = ticks_tmp[n];
      	//add color tag to
      	ticks_tmp[n]="<span class='hilite'>" + tmp + "</span>";
      	//console.log(ticks_tmp);
      	iText = ticks_tmp.join("<br />");
      	//console.log(iText);
      	document.getElementById("data_column").innerHTML=iText;
       
      	last_tick_num=timing.length;
      	duration = player.getDuration();
      	
      	 stime =timing[play_section];
      	 
      	 if ( play_section+1 > last_tick_num){
      	 	etime = duration;
      	 }else{
      	 	etime = timing[play_section+1];
      	 }
 
        player.cueVideoById({videoId:"zseRZNUX9wk",
                     startSeconds:stime,
                     endSeconds:etime });
        player.playVideo();
        //player.pauseVideo()
         
      }
      
      
      //php return the file in json
	function load_ticks (){
		$.post( "action.php", { load: 1 })
		  .done(function( data ) {
		  	console.log(data);
		  	
		  	
		  	//var ar = <?php echo json_encode($data) ?>;
		  	 
		  });

	}
	
	function save_ticks(){
		  
		fname = prompt( "Save filename as:" , "");
		
		json_data = JSON.stringify(ticks)
		
		//console.log(note_arr_fixed);
		console.log(json_data);
		
 
		$.post( "action.php", { fname : fname , data: json_data })
		  .done(function( data ) {
			alert( "Data Saved: " + data );
		  });
 
  	}