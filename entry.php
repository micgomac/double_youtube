<!DOCTYPE html>
<html>
<head> 
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="pig.css">
	<script src="dev/jquery-3.2.1.js"></script>
</head>
  <body>
    <!-- 1. The <iframe> (and video player) will replace this <div> tag. -->
    <div id="player"></div><div id="data_column" class="dataCol">---</div>
	<div id="info">---</div>
	<div class="rowbar">
		
		<!-- <div class="btn">pause</div> -->
		<input type="button" id="add_tick" onclick="tick()" value="tick">
		<input type="button" id="playPrevLoop" onclick="nextPlayLoop(-1)" value="prev section">
		<input type="button" id="playNextLoop" onclick="nextPlayLoop(1)" value="next section">
		<input type="button" id="saveTicks" onclick="save_ticks()" value="save">
		<input type="button" id="speeddown" onclick="speedchange(.5)" value="-">
		<input type="button" id="speednormal" onclick="speedchange(1)" value="+">
		<input type="button" id="mode_switch" onclick="mode_switch()" value="Mode Change">
		
		<span id = "mode_display" style="padding-right:5px;padding-left:8px;">Mode: Playback</span>
	</div>
    <script>
    
    
    (function() {
	  var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
								  window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
	  window.requestAnimationFrame = requestAnimationFrame;
	})();
    
    //be loaded from file in real
    var timing =[4.1,6.7,9.5,11.5,19.8,35.6,41.8,46.2,51.8,54.2,57.2,62.8,74.8,82.2,84.2,89.4,94.6,96.2,99.3,103.9,105.9,110.7,114.1,117.3,119.7,123.7,128.5,132.1,136.1,139.1,142.5,149.3,157.7];
    

    
    
    	var currtime_1=0;
    	var ticks =[];
    	var play_section=0;
    	var players_state = 0; //use this or api states? user set states for fast set and get
	var mode =1; //playback, 0 = entry
	var section_end_time=1000000000;
	var data_arr;
 
//===NOT WORK
var a=load_ticks();
console.log(data_arr);
//console.log(tick_data["father missing glass"]["data"]);
	
	mode_switch(); //playback
			
      // 2. This code loads the IFrame Player API code asynchronously.
      var tag = document.createElement('script');

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      // 3. This function creates an <iframe> (and YouTube player)
      //    after the API code downloads.
      var player;
      function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
          height: '390',
          width: '640',
          videoId: 'zseRZNUX9wk',
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayer1StateChange
          }
        });
        
        //set loop time event only when ready
// startInterval();
      
       
      }
      

      // 4. The API will call this function when the video player is ready.
      function onPlayerReady(event) {
        event.target.playVideo();
      }

      // 5. The API calls this function when the player's state changes.
      //    The function indicates that when playing a video (state=1),
      //    the player should play for six seconds and then stop.
      var done = false;
      
requestAnimationFrame(step);

//============= functions:

	function step() {
		//
		if ( players_state==1 ){
		 	
		 	//the timer
			var currtime = player.getCurrentTime();
      		currtime_1 = Math.round( currtime * 10 ) / 10;
      		document.getElementById("info").innerHTML=currtime_1;
      		
      		//also update data_column
      		updateData();
      		
      		//check the loop time
      		if ( currtime > section_end_time){
      			setPlayRange(); //will assign again the timings.
      		}
		}
	
		requestAnimationFrame(step);
	}
	
	
      //cant rely on this to loop
      function onPlayer1StateChange(event) {
      	
      	if (event.data == YT.PlayerState.PLAYING) {
      		players_state=1;
      		//console.log("playing");
      	}else{
      		players_state=0;
      		//console.log("stopped playing");
      	}
      	
        if (event.data == YT.PlayerState.ENDED) {
// hiliteTick(2); //alert ("done");
        }
      }
      
      function stopVideo() {
        player.stopVideo();
      }
      
      function startInterval(){
      	setInterval(function(){ 
      		var currtime = player.getCurrentTime();
      		currtime_1 = Math.round( currtime * 10 ) / 10;
      		document.getElementById("info").innerHTML=currtime_1;
      		
      		//also update data_column
      		updateData();
      		}, 200);
      }
      
      function tick(){
      	ticks.push(currtime_1);
      	ticks.sort(function(a, b){return a-b});
      	updateData();
      }
      
      function updateData(){
      	infoText = ticks.join("<br />");
      	document.getElementById("data_column").innerHTML=infoText;
      	
      	//to do: we do this even if condition no change. better change.
      	for( i=0; i<(ticks).length; i++){
			if ( currtime_1 < ticks[i] ){
			 	hiliteTick(Math.max(i-1, 0));
			 	break;
 			}
		}
      	
      }
      
      function nextPlayLoop(n){
      	
      	play_section=play_section+n;
      	if (play_section<0 ){ play_section=0 }
      	console.log(play_section);
      	//hiliteTick(play_section);
      	setPlayRange();
      }
      
      function hiliteTick(n){
		if ( n < 0 ){return;}
		
		//make a copy of ticks
		var ticks_tmp = ticks.slice(0);
		
		tmp = ticks_tmp[n];
		//add color tag to
		ticks_tmp[n]="<span class='hilite'>" + tmp + "</span>";
		//console.log(ticks_tmp);
		iText = ticks_tmp.join("<br />");
		//console.log(iText);
		document.getElementById("data_column").innerHTML=iText;
		}
		
	function setPlayRange(){
		//set play range
		
		//make sure end point, < duration
		
		last_tick_num=timing.length;
		duration = player.getDuration();
		
		 stime =timing[play_section];
		 
		 if ( play_section+1 > last_tick_num){
			etime = duration;
		 }else{
			etime = timing[play_section+1];
		 }
		//end ===make sure end point
		  
		//seekTo only handle start, I handle my own end loop checking
		player.seekTo( stime,  true);
		section_end_time=etime;
         
      }
      
      
      //php return the file in json
	function load_ticks(){
		$.post( "action.php", { load: 1 })
		  .done(function( data ) {
		  
		  	data_arr = JSON.parse(data);
		  	dd = data_arr["father missing glass"]["data"];
		  	console.log(dd);
		  	return "abc"; //dd; //data_arr;
		  });
		}
	
	function save_ticks(){
		  
		fname = prompt( "Save filename as:" , "");
		
		json_data = JSON.stringify(ticks)
		
		//console.log(note_arr_fixed);
		console.log(json_data);
		
 
		$.post( "action.php", { fname : fname , data: json_data })
		  .done(function( data ) {
			alert( "Data Saved: " + data );
		  });
 
  	}
  	
  	function mode_switch(){
  		mode=!mode;
  		switch ( mode ) {
  			case true:
  				mode_text="Entry";
				$("#saveTicks").show();
				$("#playNextLoop").hide();
				$("#playPrevLoop").hide();
				$("#add_tick").show();
				  
  				break;
  			
  			case false:
  				mode_text="Playback";
  				
				$("#saveTicks").hide();
				$("#playNextLoop").show();
				$("#playPrevLoop").show();
				$("#add_tick").hide();
				 
  				break;
  			default:
  				//alert( mode ); 
  		}
  		
  		document.getElementById("mode_display").innerHTML="Mode: "+mode_text;
  	}
  	
  	function speedchange(rate){
  		console.log(rate);
  		player.setPlaybackRate(rate);
  	}
  	
    </script>
  </body>
</html>
